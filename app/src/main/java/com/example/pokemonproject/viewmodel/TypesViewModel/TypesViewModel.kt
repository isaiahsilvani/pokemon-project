package com.example.pokemonproject.viewmodel.TypesViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.view.typesfragment.TypesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TypesViewModel @Inject constructor(val repo: com.example.pokemondata.model.PokemonRepo) : ViewModel() {

    private val _typesState: MutableLiveData<TypesState> = MutableLiveData(TypesState())
    val typesState : LiveData<TypesState> get() = _typesState

    fun getAllTypes() {
        viewModelScope.launch {
            val result = repo.getTypes()
            _typesState.value = TypesState(result)
        }
    }
}