package com.example.pokemonproject.viewmodel.PokemonDetailViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokemondata.model.PokemonRepo

class PokemonDetailVMFactory(
    private val repo: com.example.pokemondata.model.PokemonRepo
) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PokemonDetailViewModel(repo) as T
    }
}