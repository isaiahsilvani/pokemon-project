package com.example.pokemonproject.viewmodel.PokemonListViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokemondata.model.PokemonRepo

class PokemonListVMFactory(
    private val repo: com.example.pokemondata.model.PokemonRepo
) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PokeMonListViewModel(repo) as T
    }
}

//todo replace with actual repo