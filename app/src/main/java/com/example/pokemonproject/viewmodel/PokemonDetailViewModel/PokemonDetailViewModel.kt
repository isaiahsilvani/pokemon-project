package com.example.pokemonproject.viewmodel.PokemonDetailViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.view.pokedetailfragment.PokeDetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokemonDetailViewModel @Inject constructor(val repo: com.example.pokemondata.model.PokemonRepo) : ViewModel() {

    private val _pokemonDetailState: MutableLiveData<PokeDetailState> = MutableLiveData(PokeDetailState())
    val pokemonDetailState: LiveData<PokeDetailState> get() = _pokemonDetailState

    fun getPokemonDetails(name: String) {
        Log.e("DETAIL", name)
        viewModelScope.launch {
            val result = repo.getPokemonDetails(name)
            Log.e("TAGG", result.toString())
            with(_pokemonDetailState) {
                value = PokeDetailState(result)
            }
        }
    }

    fun favoritePokemon(name: String, sprite: String) {
        viewModelScope.launch {
            Log.e("DETAIL VM", "We got $name with image: $sprite")
            // repo.addFavoritePokemon(name, sprite)
            repo.likePoke(name, sprite)
        }
    }
}