package com.example.pokemonproject.viewmodel.FavoritePokemonViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokemondata.model.PokemonRepo

class FavoritePokemonDetailVMFactory(
    private val repo: com.example.pokemondata.model.PokemonRepo
) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FavoritePokemonViewModel(repo) as T
    }
}