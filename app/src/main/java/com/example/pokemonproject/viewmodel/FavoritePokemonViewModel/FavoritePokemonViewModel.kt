package com.example.pokemonproject.viewmodel.FavoritePokemonViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.view.favoritepokefragment.FavoritePokeState
import com.example.pokemonproject.view.pokelistfragment.mockFavoritePokes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoritePokemonViewModel @Inject constructor(val repo: com.example.pokemondata.model.PokemonRepo) : ViewModel() {

    private val _favoritePokeState: MutableLiveData<FavoritePokeState> =
        MutableLiveData(FavoritePokeState())
    val favoritePokeState: LiveData<FavoritePokeState> get() = _favoritePokeState

    fun getFavoritePokemon() {
        // replace with repo that makes call to backend
        viewModelScope.launch {
            val result = repo.getLiked()
            with(_favoritePokeState) {
                value = FavoritePokeState(result)
                Log.e("TAG", "getFavoritePokemon:$value ", )
            }
        }

    }


}