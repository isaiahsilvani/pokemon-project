package com.example.pokemonproject.viewmodel.PokemonListViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.util.PokemonType
import com.example.pokemonproject.view.pokelistfragment.PokeListState
import com.example.pokemonproject.view.pokelistfragment.mockPokeList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokeMonListViewModel @Inject constructor(val repo: com.example.pokemondata.model.PokemonRepo) : ViewModel() {

    private var _pokemonListState : MutableLiveData<PokeListState> = MutableLiveData(PokeListState())
    val pokemonListState : LiveData<PokeListState> get() = _pokemonListState

    fun getPokemonByType(type: String) {
        viewModelScope.launch {
            with(_pokemonListState) {

                val result = repo.getTypedPokemons(type)
                value = PokeListState(pokeNameList = result, type)
            }
        }
    }
}