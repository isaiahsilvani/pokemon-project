package com.example.pokemonproject.viewmodel.TypesViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokemondata.model.PokemonRepo

class TypesVMFactory(
    private val repo: com.example.pokemondata.model.PokemonRepo
) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TypesViewModel(repo) as T
    }
}
