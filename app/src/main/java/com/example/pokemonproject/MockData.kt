package com.example.pokemonproject.view.pokelistfragment

import com.example.pokemonproject.view.favoritepokefragment.FavoritePokeState
import com.example.pokemonproject.view.pokedetailfragment.PokeDetailState

val mockPokeList = listOf(
    PokeSearch("Squirtle", "asdfsdzfsdf"),
    PokeSearch("Pikachu", "asdfsasdfadsff"),
    PokeSearch("Charzard", "asdfasdfsdfff"),
    PokeSearch("Lizard Boy", "asdfsdzsadffsdf"),
    PokeSearch("Fart Man", "asdfsasdsadfadsfadsff"),
    PokeSearch("BOOIII", "asdfassadfdsafdfff"),
)

val mockFavoritePokes = listOf(
        "geodude", "graveler", "onix", "rhydon", "omastar"

)