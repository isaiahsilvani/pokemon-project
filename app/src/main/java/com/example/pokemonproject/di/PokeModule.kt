package com.example.pokemonproject.di

import android.content.Context
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemondata.model.local.PokemonDB
import com.example.pokemondata.model.local.PokemonDao
import com.example.pokemondata.model.remote.PokemonService
import com.example.pokemonproject.viewmodel.FavoritePokemonViewModel.FavoritePokemonDetailVMFactory
import com.example.pokemonproject.viewmodel.FavoritePokemonViewModel.FavoritePokemonViewModel
import com.example.pokemonproject.viewmodel.PokemonDetailViewModel.PokemonDetailVMFactory
import com.example.pokemonproject.viewmodel.PokemonDetailViewModel.PokemonDetailViewModel
import com.example.pokemonproject.viewmodel.PokemonListViewModel.PokeMonListViewModel
import com.example.pokemonproject.viewmodel.PokemonListViewModel.PokemonListVMFactory
import com.example.pokemonproject.viewmodel.TypesViewModel.TypesVMFactory
import com.example.pokemonproject.viewmodel.TypesViewModel.TypesViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class PokeModule {

    // Favorite Pokemon VMFactory
    @Provides
    @Singleton
    fun providesFavoritePokemonDetailVMFactory(repo: com.example.pokemondata.model.PokemonRepo): FavoritePokemonDetailVMFactory = FavoritePokemonDetailVMFactory(repo)

    // Favorite ViewModel
    @Provides
    @Singleton
    fun providesFavoriteViewmodel(vmFactory:FavoritePokemonDetailVMFactory): FavoritePokemonViewModel = vmFactory.create(FavoritePokemonViewModel::class.java)

    // Types ViewModel
    @Provides
    @Singleton
    fun providesTypesViewModel(vmFactory: TypesVMFactory): TypesViewModel = vmFactory.create(TypesViewModel::class.java)

    // Pokemon Types VMFactory
    @Provides
    @Singleton
    fun providesTypesVMFactory(repo: com.example.pokemondata.model.PokemonRepo): TypesVMFactory = TypesVMFactory(repo)

    // Pokemon List ViewModel
    @Provides
    @Singleton
    fun providesPokeMonListViewModel(vmFactory: PokemonListVMFactory): PokeMonListViewModel = vmFactory.create(PokeMonListViewModel::class.java)

    // Pokemon List VMFactory
    @Provides
    @Singleton
    fun providesPokemonListVMFactory(repo: com.example.pokemondata.model.PokemonRepo): PokemonListVMFactory = PokemonListVMFactory(repo)

    // Pokemon Detail ViewModel
    @Provides
    @Singleton
    fun providesPokemonDetailViewModel(vmFactory: PokemonDetailVMFactory): PokemonDetailViewModel = vmFactory.create(PokemonDetailViewModel::class.java)
    // Pokemon Detail VMFactory
    @Provides
    @Singleton
    fun providesPokemonDetailVMFactory(repo: com.example.pokemondata.model.PokemonRepo): PokemonDetailVMFactory = PokemonDetailVMFactory(repo)

    // REPO
    @Provides
    @Singleton
    fun providesPokemonRepo(
        pokemonDao: com.example.pokemondata.model.local.PokemonDao,
        pokemonService: com.example.pokemondata.model.remote.PokemonService
    ): com.example.pokemondata.model.PokemonRepo =
        com.example.pokemondata.model.PokemonRepo(pokemonService, pokemonDao)

    // Service
    @Provides
    @Singleton
    fun providesPokemonService(): com.example.pokemondata.model.remote.PokemonService = com.example.pokemondata.model.remote.PokemonService.getInstance()
    // DAO
    @Provides
    @Singleton
    fun providesDao(pokemonDB: com.example.pokemondata.model.local.PokemonDB): com.example.pokemondata.model.local.PokemonDao = pokemonDB.pokemonDao()
    // DATABASE
    @Provides
    @Singleton
    fun providesDatabase(@ApplicationContext context: Context): com.example.pokemondata.model.local.PokemonDB = com.example.pokemondata.model.local.PokemonDB.getInstance(context)

}