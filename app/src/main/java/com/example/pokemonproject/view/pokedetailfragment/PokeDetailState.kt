package com.example.pokemonproject.view.pokedetailfragment

import com.example.pokemondata.model.remote.PokemonDetailsResponse

data class PokeDetailState(
    val detailsResponse: com.example.pokemondata.model.remote.PokemonDetailsResponse? = null
)