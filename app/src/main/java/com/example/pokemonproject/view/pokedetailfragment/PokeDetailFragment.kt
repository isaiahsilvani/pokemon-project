package com.example.pokemonproject.view.pokedetailfragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import coil.load
import com.example.pokemonproject.databinding.FragmentPokeDetailBinding
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.viewmodel.PokemonDetailViewModel.PokemonDetailVMFactory
import com.example.pokemonproject.viewmodel.PokemonDetailViewModel.PokemonDetailViewModel
import com.example.pokemonproject.viewmodel.PokemonListViewModel.PokeMonListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PokeDetailFragment : Fragment() {

    lateinit var binding: FragmentPokeDetailBinding

    val args by navArgs<PokeDetailFragmentArgs>()

    // Create the view model
    @Inject
    lateinit var viewModel: PokemonDetailViewModel

    // Adapter
    val movesAdapter by lazy {
        PokeDetailAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPokeDetailBinding.inflate(layoutInflater)
        Log.e("DETAIL FRAG", args.id)
        viewModel.getPokemonDetails(args.id)
        // Set the recycler view for the moves
        binding.rvMovesList.adapter = movesAdapter
        binding.rvMovesList.layoutManager = GridLayoutManager(this@PokeDetailFragment.context, 2)
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.pokemonDetailState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                if (state.detailsResponse != null) {
                    with(state.detailsResponse) {
                        tvPokemonName.text = name
                        tvHeight.text = height.toString()
                        imgPokemon.load(sprites.frontDefault)
                        movesAdapter.setData(moves)
                        // Set the favorite button up here
                        btnFavorite.setOnClickListener {
                            viewModel.favoritePokemon(name, sprites.frontDefault)
                        }
                    }
                }
            }
        }
    }
}