package com.example.pokemonproject.view.pokelistfragment

import com.example.pokemondata.model.remote.Pokemon
import com.example.pokemondata.model.remote.PokemonsResponse

data class PokeListState(
    val pokeNameList: List<com.example.pokemondata.model.remote.PokemonsResponse.Pokemon> = listOf(),
    var type: String? = null
)

// This is going to be replaced by data model from data layer
class PokeSearch(
    val name: String,
    val url: String
)