package com.example.pokemonproject.view.pokelistfragment

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonproject.R
import com.example.pokemonproject.databinding.ItemPokemonSearchBinding
import com.example.pokemondata.model.remote.PokemonsResponse
import com.example.pokemonproject.util.ButtonTypeColorChanger
import com.example.pokemonproject.util.PokemonType
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PokeListAdapter(
    val navigateToDetailPage: (id: String) -> Unit,
    val context: Context,
    val type: String
) : RecyclerView.Adapter<PokeListAdapter.PokeViewHolder>() {

    var pokemonList: List<com.example.pokemondata.model.remote.PokemonsResponse.Pokemon> = listOf()
    var pokemonType: String? = null
    private val scope = CoroutineScope(Dispatchers.Default)


    inner class PokeViewHolder(binding: ItemPokemonSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var pokemonBtn = binding.btnPokemon

        fun displayItem(pokemon: com.example.pokemondata.model.remote.PokemonsResponse.Pokemon) {
            pokemonBtn.text = pokemon.pokemon.name
            scope.launch(Dispatchers.Default) {
                ButtonTypeColorChanger.changeColorByType(pokemonBtn, context, type)
            }
            pokemonBtn.setOnClickListener {
                navigateToDetailPage(pokemon.pokemon.name)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokeViewHolder {
        return PokeViewHolder(
            ItemPokemonSearchBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: PokeViewHolder, position: Int) {
        holder.displayItem(pokemonList[position])
    }

    override fun getItemCount(): Int {
        return pokemonList.size
    }

    fun setData(list: List<com.example.pokemondata.model.remote.PokemonsResponse.Pokemon>, type: String?) {
        pokemonList = list
        pokemonType = type
        notifyDataSetChanged()
    }
}