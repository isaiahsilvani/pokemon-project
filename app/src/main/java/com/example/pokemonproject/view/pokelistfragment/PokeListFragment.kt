package com.example.pokemonproject.view.pokelistfragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.pokemonproject.R
import com.example.pokemonproject.databinding.FragmentPokeListBinding
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.util.PokemonType
import com.example.pokemonproject.viewmodel.PokemonListViewModel.PokeMonListViewModel
import com.example.pokemonproject.viewmodel.PokemonListViewModel.PokemonListVMFactory
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class PokeListFragment : Fragment() {

    lateinit var binding: FragmentPokeListBinding
    val args by navArgs<PokeListFragmentArgs>()

    // Create the view model
    @Inject
    lateinit var viewModel: PokeMonListViewModel

    private val pokeAdapter by lazy {
        PokeListAdapter(
            this::navigateToDetailPage,
            requireContext(),
            args.type
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPokeListBinding.inflate(layoutInflater)
        with(binding) {
            // This is getting sent to the viewModel
            val s = resources.getString(R.string.pokemon_list_header)
            textView2.text = String.format(s, args.type.capitalize())

            rvPokeList.adapter = pokeAdapter
            rvPokeList.layoutManager = GridLayoutManager(this@PokeListFragment.context, 2)
            viewModel.getPokemonByType(args.type)
            val header = "${args.type.replaceFirstChar { it.uppercase() }} Pokemon"
            binding.textView2.text = header
            initObservers()
        }
        return binding.root
    }

    fun String.capitalize() =
        replaceFirstChar { firstChar -> if (firstChar.isLowerCase()) firstChar.titlecase(Locale.ROOT) else firstChar.toString() }

    private fun initObservers() {
        viewModel.pokemonListState.observe(viewLifecycleOwner) { state ->
            with(state) {
                Log.e("TAGG", state.pokeNameList.toString())
                pokeAdapter.setData(pokeNameList, type)
            }
        }
    }

    fun navigateToDetailPage(pokeName: String) {
        val action = PokeListFragmentDirections.actionPokeListFragmentToPokeDetailFragment(pokeName)
        findNavController().navigate(action)
    }
}