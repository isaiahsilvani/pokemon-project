package com.example.pokemonproject.view.favoritepokefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pokemonproject.databinding.FragmentFavoritePokeBinding
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.view.pokelistfragment.PokeListFragment
import com.example.pokemonproject.view.pokelistfragment.PokeListFragmentDirections
import com.example.pokemonproject.viewmodel.FavoritePokemonViewModel.FavoritePokemonDetailVMFactory
import com.example.pokemonproject.viewmodel.FavoritePokemonViewModel.FavoritePokemonViewModel
import com.example.pokemonproject.viewmodel.PokemonDetailViewModel.PokemonDetailVMFactory
import com.example.pokemonproject.viewmodel.PokemonDetailViewModel.PokemonDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class FavoritePokeFragment : Fragment() {

    lateinit var binding: FragmentFavoritePokeBinding
    // Create the view model
    @Inject
    lateinit var viewModel: FavoritePokemonViewModel

    val favsAdapter by lazy {
        FavoritePokeAdapter(
            this::navigateToDetailPage
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritePokeBinding.inflate(layoutInflater)
        viewModel.getFavoritePokemon()
        // Set up the adapter
        binding.rvFavoritePokemon.adapter = favsAdapter
        binding.rvFavoritePokemon.layoutManager = LinearLayoutManager(this@FavoritePokeFragment.context)
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.favoritePokeState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                if (!state.favoritePokes.isNullOrEmpty()) {
                    favsAdapter.setData(state.favoritePokes)
                }
            }
        }
    }

    private fun navigateToDetailPage(pokeName: String) {
        val action = FavoritePokeFragmentDirections.actionFavoritePokeFragmentToPokeDetailFragment(pokeName)
        findNavController().navigate(action)
    }
}