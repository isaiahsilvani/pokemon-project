package com.example.pokemonproject.view.pokedetailfragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonproject.databinding.ItemMovesBinding
import com.example.pokemondata.model.remote.PokemonDetailsResponse

class PokeDetailAdapter : RecyclerView.Adapter<PokeDetailAdapter.MoveViewHolder>() {

    private var moveList: List<com.example.pokemondata.model.remote.PokemonDetailsResponse.Move> = listOf()

    inner class MoveViewHolder(binding: ItemMovesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val moveName = binding.tvMove
        fun displayItem(move: com.example.pokemondata.model.remote.PokemonDetailsResponse.Move) {
            moveName.text = move.move.name
        }
    }

    fun setData(list: List<com.example.pokemondata.model.remote.PokemonDetailsResponse.Move>) {
        moveList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PokeDetailAdapter.MoveViewHolder {
        return MoveViewHolder(
            ItemMovesBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: PokeDetailAdapter.MoveViewHolder, position: Int) {
        holder.displayItem(moveList[position])
    }

    override fun getItemCount(): Int = moveList.size
}