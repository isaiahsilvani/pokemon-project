package com.example.pokemonproject.view.typesfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pokemonproject.databinding.FragmentTypesBinding
import com.example.pokemondata.model.PokemonRepo
import com.example.pokemonproject.view.pokelistfragment.PokeListFragmentDirections
import com.example.pokemonproject.viewmodel.TypesViewModel.TypesVMFactory
import com.example.pokemonproject.viewmodel.TypesViewModel.TypesViewModel
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TypesFragment : Fragment() {

    lateinit var binding: FragmentTypesBinding

    // Create the view model
    @Inject
    lateinit var viewModel:TypesViewModel
    private val typesAdapter by lazy {
        TypesAdapter(
            this::navigateToPokemonList,
            requireContext()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTypesBinding.inflate(layoutInflater)
        viewModel.getAllTypes()
        // Recycler View
        binding.rvTypes.adapter = typesAdapter
        binding.rvTypes.layoutManager = GridLayoutManager(this@TypesFragment.context, 2)
        initObservers()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.tvHeader.setOnClickListener {
            val action = TypesFragmentDirections.actionTypesFragmentToFavoritePokeFragment()
            findNavController().navigate(action)
        }
    }

    private fun initObservers() {
        viewModel.typesState.observe(viewLifecycleOwner) { state ->
            typesAdapter.setData(state.typeList)
        }
    }

    private fun navigateToPokemonList(type: String) {
        val action = TypesFragmentDirections.actionTypesFragmentToPokeListFragment(type)
        findNavController().navigate(action)
    }
}