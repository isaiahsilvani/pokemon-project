package com.example.pokemonproject.view.typesfragment

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonproject.databinding.ItemTypeBinding
import com.example.pokemondata.model.remote.Result
import com.example.pokemonproject.util.ButtonTypeColorChanger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TypesAdapter(
    val navigateToPokemonList: (type: String) -> Unit,
    val context: Context
) : RecyclerView.Adapter<TypesAdapter.TypeViewHolder>() {

    private var typeList: List<com.example.pokemondata.model.remote.Result> = listOf()
    private val scope = CoroutineScope(Dispatchers.Default)

    inner class TypeViewHolder(private val binding: ItemTypeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var btnType = binding.btnType
        fun displayItem(type: String) {
            btnType.text = type
            scope.launch(Dispatchers.Default) {
                ButtonTypeColorChanger.changeColorByType(btnType, context)
            }
            btnType.setOnClickListener {
                navigateToPokemonList(type)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypesAdapter.TypeViewHolder {
        return TypeViewHolder(
            ItemTypeBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: TypesAdapter.TypeViewHolder, position: Int) {
        holder.displayItem(typeList[position].name)
    }

    override fun getItemCount(): Int = typeList.size

    fun setData(list: List<com.example.pokemondata.model.remote.Result>) {
        typeList = list
        notifyDataSetChanged()
    }
}