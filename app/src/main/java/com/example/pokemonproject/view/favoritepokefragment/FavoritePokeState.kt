package com.example.pokemonproject.view.favoritepokefragment

import com.example.pokemondata.model.local.PokemonEntity

data class FavoritePokeState(
    val favoritePokes : List<com.example.pokemondata.model.local.PokemonEntity>? = null
)