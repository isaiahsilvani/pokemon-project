package com.example.pokemonproject.view.typesfragment

import com.example.pokemondata.model.remote.Result

data class TypesState(
    val typeList: List<com.example.pokemondata.model.remote.Result> = listOf()
)
