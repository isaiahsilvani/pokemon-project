package com.example.pokemonproject.view.favoritepokefragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonproject.databinding.ItemMovesBinding
import com.example.pokemondata.model.local.PokemonEntity

class FavoritePokeAdapter(
    val navigateToDetailPage: (id: String) -> Unit
) : RecyclerView.Adapter<FavoritePokeAdapter.FavPokeViewHolder>() {

    private var favoritePokeList : List<com.example.pokemondata.model.local.PokemonEntity> = listOf()

    inner class FavPokeViewHolder(private val binding: ItemMovesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val pokeName = binding.tvMove
        fun displayItem(item: com.example.pokemondata.model.local.PokemonEntity) {
            pokeName.text = item.name
            pokeName.setOnClickListener {
                navigateToDetailPage(item.name)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavPokeViewHolder {
        return FavPokeViewHolder(
            ItemMovesBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: FavPokeViewHolder, position: Int) {
        holder.displayItem(favoritePokeList[position])
    }

    override fun getItemCount(): Int {
        return favoritePokeList.size
    }

    fun setData(list: List<com.example.pokemondata.model.local.PokemonEntity>) {
        favoritePokeList = list
        notifyDataSetChanged()
    }

}