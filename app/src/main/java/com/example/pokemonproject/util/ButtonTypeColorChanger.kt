package com.example.pokemonproject.util

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.Log
import androidx.core.content.ContextCompat
import com.example.pokemonproject.R
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ButtonTypeColorChanger {


    suspend fun changeColorByType(button: MaterialButton, context: Context, type: String? = null) = withContext(Dispatchers.Default){
        button.backgroundTintList = when (type ?: button.text) {
            "normal" -> getColor(R.color.normal, context)
            "flying" -> getColor(R.color.flying, context)
            "psychic" -> getColor(R.color.psychic, context)
            "ground" -> getColor(R.color.ground, context)
            "poison" -> getColor(R.color.poisin, context)
            "rock" -> getColor(R.color.rock, context)
            "bug" -> getColor(R.color.bug, context)
            "ghost" -> getColor(R.color.ghost, context)
            "steel" -> getColor(R.color.steel, context)
            "electric" -> getColor(R.color.electric_yellow, context)
            "fire" -> getColor(R.color.fire_red, context)
            "fighting" -> getColor(R.color.fighting, context)
            "grass" -> getColor(R.color.grass_green, context)
            "water" -> getColor(R.color.water_blue, context)
            "ice" -> getColor(R.color.ice, context)
            "dragon" -> getColor(R.color.dragon, context)
            "unknown" -> getColor(R.color.unknown, context)
            "shadow" -> getColor(R.color.shadow, context)
            "dark" -> getColor(R.color.dark, context)
            "fairy"-> getColor(R.color.fairy, context)
            else -> null
        }
    }

    private fun getColor(colorId: Int, context: Context) : ColorStateList {
        return ColorStateList.valueOf(ContextCompat.getColor(context, colorId))
    }


}