package com.example.pokemonproject.util

enum class PokemonType {
    WATER, ELECTRIC, GRASS, FIRE
}