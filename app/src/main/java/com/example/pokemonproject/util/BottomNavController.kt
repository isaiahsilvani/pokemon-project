package com.example.pokemonproject.util

import androidx.navigation.NavController
import com.example.pokemonproject.R
import com.example.pokemonproject.view.favoritepokefragment.FavoritePokeFragmentDirections
import com.example.pokemonproject.view.pokedetailfragment.PokeDetailFragmentDirections
import com.example.pokemonproject.view.pokelistfragment.PokeListFragmentDirections
import com.example.pokemonproject.view.typesfragment.TypesFragmentDirections

class BottomNavController(
    private val navController: NavController
) {

    fun setNavigationTab(fragmentId: Int): Boolean {
        // Seperate navigation by fragmentId

        when (fragmentId) {
            R.id.typesFragment -> setTypesTab()
            R.id.favoritePokeFragment -> setFavoriteTab()
        }
        return true
    }

    private fun setTypesTab() {

        when (navController.currentDestination?.label.toString()) {

            "PokeListFragment" -> {
                val action = PokeListFragmentDirections.actionPokeListFragmentToTypesFragment()
                navController.navigate(action)
            }
            "favoritePokeFragment" -> {
                val action =
                    FavoritePokeFragmentDirections.actionFavoritePokeFragmentToTypesFragment()
                navController.navigate(action)
            }
            "PokeDetailFragment" -> {
                val action = PokeDetailFragmentDirections.actionPokeDetailFragmentToTypesFragment()
                navController.navigate(action)
            }
        }
    }

    private fun setFavoriteTab() {

        when (navController.currentDestination?.label.toString()) {
            "PokeListFragment" -> {
                val action = PokeListFragmentDirections.actionPokeListFragmentToFavoritePokeFragment()
                navController.navigate(action)
            }

            "TypesFragment" -> {
                val action = TypesFragmentDirections.actionTypesFragmentToFavoritePokeFragment()
                navController.navigate(action)
            }

            "PokeDetailFragment" -> {
                val action = PokeDetailFragmentDirections.actionPokeDetailFragmentToFavoritePokeFragment()
                navController.navigate(action)
            }
        }
    }
}
