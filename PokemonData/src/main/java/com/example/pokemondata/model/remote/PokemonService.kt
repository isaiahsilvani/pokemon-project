package com.example.pokemondata.model.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {
    companion object {
        private const val BASE_URL:String = "https://pokeapi.co/api/v2/"
        private const val ENDPOINTTYPE = "type/"
        private const val ENDPOINTTYPEPOKEMON = "type/{type}"
        private const val ENDPOINTPOKEMON = "pokemon/{name}"
        // https://pokeapi.co/api/v2/type/water

        private val interceptor = HttpLoggingInterceptor()
            .also {
                it.setLevel(HttpLoggingInterceptor.Level.BODY)
            }

        private val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

        fun getInstance(): PokemonService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(ENDPOINTTYPE)
    suspend fun getTypes(): TypesResponse

    @GET(ENDPOINTTYPEPOKEMON)
    suspend fun getTypedPokemons(@Path("type") type: String): PokemonsResponse

    @GET(ENDPOINTPOKEMON)
    suspend fun getPokemonDetails(@Path("name") name: String): PokemonDetailsResponse


}
