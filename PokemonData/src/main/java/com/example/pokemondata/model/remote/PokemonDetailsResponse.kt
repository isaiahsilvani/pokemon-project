package com.example.pokemondata.model.remote


import com.google.gson.annotations.SerializedName

data class PokemonDetailsResponse(

    @SerializedName("height")
    val height: Int,
    @SerializedName("moves")
    val moves: List<Move>,
    @SerializedName("sprites")
    val sprites: Sprites,
    @SerializedName("name")
    val name: String,
    @SerializedName("weight")
    val weight: Int
){
    data class Move(
        @SerializedName("move")
        val move: MoveX,

        )
    data class MoveX(
        @SerializedName("name")
        val name: String,
        @SerializedName("url")
        val url: String
    )

    data class Sprites(
//        @SerializedName("back_default")
//        val backDefault: String,
//        @SerializedName("back_female")
//        val backFemale: String,
//        @SerializedName("back_shiny")
//        val backShiny: String,
//        @SerializedName("back_shiny_female")
//        val backShinyFemale: String,
        @SerializedName("front_default")
        val frontDefault: String,
//        @SerializedName("front_female")
//        val frontFemale: String,
//        @SerializedName("front_shiny")
//        val frontShiny: String,
//        @SerializedName("front_shiny_female")
//        val frontShinyFemale: String,
//        @SerializedName("other")
//        val other: Other,
//        @SerializedName("versions")
//        val versions: Versions
    )
}