package com.example.pokemondata.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.pokemondata.model.remote.PokemonsResponse


@Database(entities=[PokemonEntity::class], version = 1)
abstract class PokemonDB:RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
    companion object{
        private const val DATABASE_NAME = "pokemon.db"

        @Volatile
        private var instance: PokemonDB?= null

        fun getInstance(context: Context): PokemonDB {
            return instance ?: synchronized(this){
                instance ?: buildDatabase(context).also{
                    instance = it
                }
            }
        }
        private fun buildDatabase(context: Context): PokemonDB {
            return Room.databaseBuilder(
                context.applicationContext,
                PokemonDB::class.java,
                DATABASE_NAME
            ).build()
        }
    }
}