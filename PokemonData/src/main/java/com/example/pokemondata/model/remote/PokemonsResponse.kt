package com.example.pokemondata.model.remote


import com.google.gson.annotations.SerializedName

data class PokemonsResponse(
    @SerializedName("pokemon")
    val pokemon: List<Pokemon>
){
    data class Pokemon(
        @SerializedName("pokemon")
        val pokemon: PokemonX,
        @SerializedName("slot")
        val slot: Int
    )


    data class PokemonX(
        @SerializedName("name")
        val name: String,
        @SerializedName("url")
        val url: String
    )
}