package com.example.pokemondata.model

import com.example.pokemondata.model.local.PokemonDao
import com.example.pokemondata.model.local.PokemonEntity
import com.example.pokemondata.model.remote.PokemonService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PokemonRepo @Inject constructor(
    private val pokemonService: PokemonService,
    private val pokemonDao: PokemonDao
) {

    suspend fun getTypes() = withContext(Dispatchers.IO) {
        return@withContext pokemonService.getTypes().results
    }

    suspend fun getTypedPokemons(type:String) = withContext(Dispatchers.IO){
        return@withContext pokemonService.getTypedPokemons(type).pokemon
    }

    suspend fun getPokemonDetails(name:String) = withContext(Dispatchers.IO){
        return@withContext pokemonService.getPokemonDetails(name)
    }

    suspend fun getLiked():List<PokemonEntity>? = withContext(Dispatchers.IO){
        val likedPokes =pokemonDao.getFavorites()
        likedPokes.ifEmpty { return@ifEmpty null }
    }

    suspend fun likePoke(name:String, sprite:String) = withContext(Dispatchers.IO){
        pokemonDao.insertPokemon(PokemonEntity(name = name, sprite= sprite))
    }
}