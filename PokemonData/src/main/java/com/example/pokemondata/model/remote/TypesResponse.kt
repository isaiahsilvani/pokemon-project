package com.example.pokemondata.model.remote


import com.google.gson.annotations.SerializedName

data class TypesResponse(
    @SerializedName("results")
    val results: List<Result>
)
