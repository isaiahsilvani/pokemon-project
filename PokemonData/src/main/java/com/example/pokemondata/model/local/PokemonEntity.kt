package com.example.pokemondata.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PokemonEntity(
   @PrimaryKey val name:String= "",
    val sprite:String= ""
)