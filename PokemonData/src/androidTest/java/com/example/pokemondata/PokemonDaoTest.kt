package com.example.pokemondata

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.pokemondata.model.local.PokemonDB
import com.example.pokemondata.model.local.PokemonDao
import com.example.pokemondata.model.local.PokemonEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import com.google.common.truth.Truth.assertThat

@RunWith(AndroidJUnit4::class)
class PokemonDaoTest {

    lateinit var db: PokemonDB
    lateinit var dao: PokemonDao

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, PokemonDB::class.java
        ).build()
        dao = db.pokemonDao()
    }

    @After
    fun teardown() {
        db.close()
    }

    @Test
    fun insertItemToDatabase() = runBlocking {
        val favPokemon = PokemonEntity("Pika", "urlafdfa")
        dao.insertPokemon(favPokemon)
        val allFavorites = dao.getFavorites()
        assertThat(allFavorites.contains(favPokemon)).isTrue()
    }


}